/* Quizás no todos los includes sean necesarios */
#include <stdio.h>      /* printf, scanf, NULL */
#include <stdlib.h>     /* malloc, free, rand */
#include <string.h>
#include <assert.h>
#include <cstring>
#include <memory>
#include <vector>
#include <fstream>
#include <openssl/aes.h>
#include <openssl/evp.h>
#include <openssl/rsa.h>
#include <openssl/pem.h>
#include <openssl/ssl.h>
#include <openssl/bio.h>
#include <openssl/err.h>
#include <openssl/sha.h> 
#include <openssl/engine.h>

/* BASE 64 DEFINITIONS */
/*
 * Base64 encoding/decoding (RFC1341)
 * Copyright (c) 2005-2011, Jouni Malinen <j@w1.fi>
 *
 * This software may be distributed under the terms of the BSD license.
 * See README for more details.
 */
static const unsigned char base64_table[65] =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";

/**
 * base64_encode - Base64 encode
 * @src: Data to be encoded
 * @len: Length of the data to be encoded
 * @out_len: Pointer to output length variable, or %NULL if not used
 * Returns: Allocated buffer of out_len bytes of encoded data,
 * or %NULL on failure
 *
 * Caller is responsible for freeing the returned buffer. Returned buffer is
 * nul terminated to make it easier to use as a C string. The nul terminator is
 * not included in out_len.
 */
unsigned char * base64_encode(const unsigned char *src, size_t len,
                  size_t *out_len)
{
    unsigned char *out, *pos;
    const unsigned char *end, *in;
    size_t olen;
    int line_len;

    olen = len * 4 / 3 + 4; /* 3-byte blocks to 4-byte */
    olen += olen / 72; /* line feeds */
    olen++; /* nul termination */
    if (olen < len)
        return NULL; /* integer overflow */
    out = (unsigned char*) malloc(olen);
    if (out == NULL)
        return NULL;

    end = src + len;
    in = src;
    pos = out;
    line_len = 0;
    while (end - in >= 3) {
        *pos++ = base64_table[in[0] >> 2];
        *pos++ = base64_table[((in[0] & 0x03) << 4) | (in[1] >> 4)];
        *pos++ = base64_table[((in[1] & 0x0f) << 2) | (in[2] >> 6)];
        *pos++ = base64_table[in[2] & 0x3f];
        in += 3;
        line_len += 4;
        if (line_len >= 72) {
            *pos++ = '\n';
            line_len = 0;
        }
    }

    if (end - in) {
        *pos++ = base64_table[in[0] >> 2];
        if (end - in == 1) {
            *pos++ = base64_table[(in[0] & 0x03) << 4];
            *pos++ = '=';
        } else {
            *pos++ = base64_table[((in[0] & 0x03) << 4) |
                          (in[1] >> 4)];
            *pos++ = base64_table[(in[1] & 0x0f) << 2];
        }
        *pos++ = '=';
        line_len += 4;
    }

    if (line_len)
        *pos++ = '\n';

    *pos = '\0';
    if (out_len)
        *out_len = pos - out;
    return out;
}


/**
 * base64_decode - Base64 decode
 * @src: Data to be decoded
 * @len: Length of the data to be decoded
 * @out_len: Pointer to output length variable
 * Returns: Allocated buffer of out_len bytes of decoded data,
 * or %NULL on failure
 *
 * Caller is responsible for freeing the returned buffer.
 */
unsigned char * base64_decode(const unsigned char *src, size_t len,size_t *out_len)
{
    unsigned char dtable[256], *out, *pos, block[4], tmp;
    size_t i, count, olen;
    int pad = 0;

    memset(dtable, 0x80, 256);
    for (i = 0; i < sizeof(base64_table) - 1; i++)
        dtable[base64_table[i]] = (unsigned char) i;
    dtable['='] = 0;

    count = 0;
    for (i = 0; i < len; i++) {
        if (dtable[src[i]] != 0x80)
            count++;
    }

    if (count == 0 || count % 4)
        return NULL;

    olen = count / 4 * 3;
    pos = out = (unsigned char*)malloc(olen);
    if (out == NULL)
        return NULL;

    count = 0;
    for (i = 0; i < len; i++) {
        tmp = dtable[src[i]];
        if (tmp == 0x80)
            continue;

        if (src[i] == '=')
            pad++;
        block[count] = tmp;
        count++;
        if (count == 4) {
            *pos++ = (block[0] << 2) | (block[1] >> 4);
            *pos++ = (block[1] << 4) | (block[2] >> 2);
            *pos++ = (block[2] << 6) | block[3];
            count = 0;
            if (pad) {
                if (pad == 1)
                    pos--;
                else if (pad == 2)
                    pos -= 2;
                else {
                    /* Invalid padding */

                    free(out);
                    return NULL;
                }
                break;
            }
        }
    }

    *out_len = pos - out;
    return out;
}

/*
 Firma un hash creado con SHA256 usando una llave privada en formato DER (binario) cuya ruta esta definida por pvtKey_file
 el passpharse de la llave privada por default es un apuntador NULL.
 
 El resultado de la firma es devuelto en un arreglo de usigned char* y NULL si algún error ocurrió.
*/
unsigned char* signHash_DER(std::string hash_str, std::string privateKey_file, char * passphrase /*= NULL*/){
	EVP_PKEY * pkey = PKCS8_DERtoEVP(privateKey_file,passphrase);
	EVP_PKEY_CTX *ctx;
	unsigned char *sig;
	size_t siglen;
	size_t mdlen;
	if (pkey == NULL) {
		LOGF_TRACE("Error creando EVP_PKEY");
		return NULL;
		}

	unsigned char clear_message[hash_str.length()];
  	strcpy((char*) clear_message,hash_str.c_str());
	unsigned char * md = base64_decode(clear_message, strlen((char*) clear_message), &mdlen);
	ctx = EVP_PKEY_CTX_new(pkey, ENGINE_get_default_RSA() /* no engine */);
	if(!ctx) {
	LOGF_ERROR("Error CTX_new");
    return NULL;
  }
  if (EVP_PKEY_sign_init(ctx) <= 0){
    LOGF_ERROR("Error sign_init\n");
    return NULL;
  }
 if (EVP_PKEY_CTX_set_rsa_padding(ctx, RSA_PKCS1_PADDING) <= 0){
    LOGF_ERROR("Error set_rsa_padding\n");
    return NULL;
 }
 if (EVP_PKEY_CTX_set_signature_md(ctx, EVP_sha256()) <= 0){
    LOGF_ERROR("Error set_signature_md\n");
    return NULL;
 }

 /* Determine buffer length */
 if (EVP_PKEY_sign(ctx, NULL, &siglen, md, mdlen) <= 0){
    LOGF_ERROR("Error PKEY_sign\n");
    return NULL;
 }
 sig = (unsigned char*)OPENSSL_malloc(siglen);

 if (!sig){
    LOGF_ERROR("Error malloc");
    return NULL;
 }

 if (EVP_PKEY_sign(ctx, sig, &siglen, md, mdlen) <= 0){
    std::cout << "Error sign";
    return NULL;
 }
 /* Signature is siglen bytes written to buffer sig */
 // La cadena sellada debe ser codificada en base64.
 size_t cadena_sellada_len;
 unsigned char * cadena_sellada = base64_encode(sig, siglen, &cadena_sellada_len);

 return cadena_sellada;
}

/*
PKCS8_DERtoEVP(string)
Dada una ruta de una llave privada en formato binario (DER PKCS8) 
devuelve un apuntador EVP_PKEY cuya llave asociada es la llave DER.
*/
EVP_PKEY * PKCS8_DERtoEVP(std::string der_key_file, char * pwd){
	
// Recibe la ruta de una llave privada RSA con formato DER (bytes).
// La transforma a formato PEM (en memoria) y la guarda en private_der.
// private_der debe ser un puntero sin inicializar.
	FILE * fp = NULL;
	EVP_PKEY * evp_pvt = NULL;
	OpenSSL_add_all_algorithms();
	fp = fopen(der_key_file.c_str(),"rb");
	if( fp ){
		//char * pwd = "12345678a";
		evp_pvt = d2i_PKCS8PrivateKey_fp(fp, &evp_pvt, pwd_cb, pwd);
		if (evp_pvt == NULL) {
			LOGF_TRACE("Error d2i_PKCS8PrivateKey_fp...%s",ERR_error_string(ERR_get_error(), NULL) );
			fclose(fp);
			return NULL ;
			}
	}
	LOGF_TRACE("Created evp_pvt");
	fclose(fp);
	return evp_pvt;
	/* Estas lineas sirven para imprimir algo de información de la llave privada.
	BIO * mem = BIO_new(BIO_s_mem());
	EVP_PKEY_print_private(mem, evp_pvt, 0, NULL);
	char data [3096];
	LOGF_TRACE("Attemping to print pvt key data");
	BIO_read(mem, data,3096);
	LOGF_TRACE("%s",data);
	BIO_free(mem);
	*/
}


/*DRIVER*/

int main(){
    unsigned char* signature_decoded = signHash_DER(cadena_original, "flash/EKU9003173C9.key","12345678a");
	std::string selladofinal((char*)signature_decoded);
	LOGF_TRACE("Longitud de Sellado: %d", strlen((char*)signature_decoded));
	LOGF_TRACE("Sellado normal: ");
	LOGF_TRACE("%s",selladofinal.c_str());
}
