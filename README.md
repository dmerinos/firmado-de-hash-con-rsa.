# README #
Para firmar una cadena de texto que debe ser un hash creado con SHA256.
Este código puede ser modificado para firmar otras cadenas de texto creadas con diferentes algoritmos de hasheo.

# Dependencias # 
- Se requiere la librería OpenSSL en su versión 1.1.X o superior.
- Cualquier versión de g++ debería compilar sin problemas, nosotros hemos compilado con versiones anteriores a C++11.
- Las llamadas #LOGF_TRACE# son propietarias pero equivalen a un printf().


